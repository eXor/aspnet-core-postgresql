﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Backend.Data;

namespace api.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20160729105245_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.0.0-rtm-21431");

            modelBuilder.Entity("Backend.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Emailaddress");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("Backend.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<int?>("PaymentId");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("PaymentId");

                    b.ToTable("Orders");

                    b.HasDiscriminator<string>("Discriminator").HasValue("Order");
                });

            modelBuilder.Entity("Backend.Models.OrderRecording", b =>
                {
                    b.Property<int>("OrderId");

                    b.Property<int>("RecordingId");

                    b.HasKey("OrderId", "RecordingId");

                    b.HasIndex("OrderId");

                    b.HasIndex("RecordingId");

                    b.ToTable("OrderRecording");
                });

            modelBuilder.Entity("Backend.Models.Payment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<double>("Cost");

                    b.Property<DateTime>("DateCreated");

                    b.Property<bool>("FreeOfCharge");

                    b.Property<bool>("IsPayed");

                    b.Property<DateTime?>("PayDate");

                    b.HasKey("Id");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("Backend.Models.Recording", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Date");

                    b.Property<DateTime>("DateCreated");

                    b.Property<string>("FileLocation");

                    b.HasKey("Id");

                    b.ToTable("Recordings");
                });

            modelBuilder.Entity("Backend.Models.Subscription", b =>
                {
                    b.HasBaseType("Backend.Models.Order");

                    b.Property<DateTime>("BeginDate");

                    b.Property<DateTime>("EndDate");

                    b.ToTable("Subscription");

                    b.HasDiscriminator().HasValue("Subscription");
                });

            modelBuilder.Entity("Backend.Models.Order", b =>
                {
                    b.HasOne("Backend.Models.Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId");

                    b.HasOne("Backend.Models.Payment", "Payment")
                        .WithMany()
                        .HasForeignKey("PaymentId");
                });

            modelBuilder.Entity("Backend.Models.OrderRecording", b =>
                {
                    b.HasOne("Backend.Models.Order", "Order")
                        .WithMany("OrderRecordings")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Backend.Models.Recording", "Recording")
                        .WithMany("OrderRecordings")
                        .HasForeignKey("RecordingId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
