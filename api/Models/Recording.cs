using System;
using System.Collections.Generic;

namespace Backend.Models
{
    public class Recording : BaseModel
    {
        public Recording()
        {
            OrderRecordings = new List<OrderRecording>();
        }

        public DateTime Date { get; set; }
        public string FileLocation { get; set; }
        public virtual List<OrderRecording> OrderRecordings { get; set; }
    }
}