using Backend.Controllers.Abstract;
using Backend.Controllers.ViewModels;
using Backend.Data;
using Backend.Models;

namespace Backend.Controllers
{
    public class SubscriptionsController : GenericController<Subscription, SubscriptionBaseModel, SubscriptionViewModel>
    {
        public SubscriptionsController(ApplicationDbContext context) : base(context, context.Subscriptions)
        { }
    }
}