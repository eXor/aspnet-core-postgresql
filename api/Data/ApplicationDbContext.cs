using Microsoft.EntityFrameworkCore;
using Backend.Models;
using OpenIddict;

namespace Backend.Data
{
    public class ApplicationDbContext : OpenIddictDbContext<ApplicationUser>
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Recording> Recordings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OrderRecording>()
                .HasKey(koppelTabel => new { koppelTabel.OrderId, koppelTabel.RecordingId });

            modelBuilder.Entity<OrderRecording>()
                .HasOne(or => or.Order)
                .WithMany(order => order.OrderRecordings)
                .HasForeignKey(or => or.OrderId);

            modelBuilder.Entity<OrderRecording>()
                .HasOne(or => or.Recording)
                .WithMany(p => p.OrderRecordings)
                .HasForeignKey(pt => pt.RecordingId);
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options) { }
    }
}
