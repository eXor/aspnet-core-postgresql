using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Backend.Data;
using Backend.Models;
using Swashbuckle.Swagger.Model;
using System.Linq;
using CryptoHelper;
using OpenIddict;

namespace Backend
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            // Add database
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseNpgsql(Configuration["Data:Connectionstring"]));

            // Add Identity services
            services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();

            // Register OpenIddict services, with default Entity Framework stores
            services.AddOpenIddict<ApplicationUser, ApplicationDbContext>()
                // Enable the token endpoint (required to use password flow)
                .EnableAuthorizationEndpoint("/connect/authorize")
                .EnableTokenEndpoint("/connect/token")

                // Allow client applications to use the grand_type=password flow
                .AllowPasswordFlow()
                .AllowAuthorizationCodeFlow()

                // During dev, disable https
                .DisableHttpsRequirement()

                // Register a new ephemeral key, that is discarded when the application
                // shuts down. Tokens signed using this key are automatically invalidated.
                // This method should only be used during development.
                .AddEphemeralSigningKey();

            // Add Cors
            services.AddCors();

            // Add Swashbuckle
            services.AddSwaggerGen();
            services.ConfigureSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SingleApiVersion(new Info
                {
                    Title = "Gemplan-audio API",
                    Version = "v1",
                    Description = "The API for gemplan-audio",
                    TermsOfService = "None"
                });
            });

            // Add application services
            // services.AddScoped<ITodoService, TodoService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            // Simple error page to avoid a repo dependency.
            app.Use(async (context, next) =>
            {
                try
                {
                    await next();
                }
                catch (Exception ex)
                {
                    if (context.Response.HasStarted)
                    {
                        throw;
                    }
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync(ex.ToString());
                }
            });

            // Temporary static files from wwwroot
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseOAuthValidation();

            app.UseIdentity();

            app.UseGitHubAuthentication(options =>
            {
                options.ClientId = Configuration["oauth:github:clientid"];
                options.ClientSecret = Configuration["oauth:github:clientsecret"];
                // options.Scope.Add("user:email");
                // options.AutomaticAuthenticate = false;
                // options.AutomaticChallenge = false;
            });

            // Use OpenIddict after UseIdentity and External Social Providers
            app.UseOpenIddict();

            // Use Cors
            app.UseCors(builder =>
                builder.WithOrigins("http://localhost:4200")
                    .AllowAnyHeader()
                    .AllowAnyMethod()
            );

            // Use Mvc
            app.UseMvc();

            // Use Swagger
            app.UseSwagger();
            app.UseSwaggerUi();

            // Register client application
            using (var context = new ApplicationDbContext(app.ApplicationServices.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                context.Database.EnsureCreated();

                if (!context.Applications.Any())
                {
                    context.Applications.Add(new OpenIddictApplication
                    {
                        // Assign a unique identifier to your client app:
                        Id = "61ecf6c8-eb73-4f58-86f0-b7d370cdef57",

                        // Assign a display named used in the consent form page:
                        DisplayName = "Gemplan Audio",

                        // Register the appropriate redirect_uri and post_logout_redirect_uri:
                        RedirectUri = "http://localhost:5000/signin-oidc",
                        LogoutRedirectUri = "http://localhost:5000/",

                        // Generate a new derived key from the client secret:
                        // ClientSecret only needed for confidential (non-public) client types
                        // ClientSecret = Crypto.HashPassword("secret_secret_secret"),

                        // Note: use "public" for JS/mobile/desktop applications
                        // and "confidential" for server-side applications.
                        Type = OpenIddictConstants.ClientTypes.Public
                    });

                    context.SaveChanges();
                }
            }

        }
    }
}
