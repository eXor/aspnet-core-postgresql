using System;

namespace Backend.Controllers.ViewModels
{
    public class RecordingBaseModel
    {
        public DateTime Date { get; set; }
        public string FileLocation { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;

    }

    public class RecordingViewModel : RecordingBaseModel
    {
        public int Id { get; set; }
    }
}