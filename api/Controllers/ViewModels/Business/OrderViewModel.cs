using System;

namespace Backend.Controllers.ViewModels
{
    public class OrderBaseModel
    {
        public int CustomerId { get; set; }
        public DateTime DateCreated { get; set; } = DateTime.Now;
    }

    public class OrderViewModel : OrderBaseModel
    {
        public int Id { get; set; }
        public string Customer { get { return $"/api/Customers/{CustomerId}"; } }
    }
}