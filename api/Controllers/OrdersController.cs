using Backend.Data;
using Backend.Models;
using Backend.Controllers.Abstract;
using Microsoft.AspNetCore.Mvc;
using Backend.Controllers.ViewModels;
using Microsoft.AspNetCore.Routing;

namespace Backend.Controllers
{
    [Route("api/Customers/{CustomerId:int}/[controller]")]
    public class OrdersController : GenericController<Order, OrderBaseModel, OrderViewModel>
    {
        public OrdersController(ApplicationDbContext context) : base(context, context.Orders)
        {
            expression = (httpContext) => (order) => order.CustomerId == int.Parse((string)httpContext.GetRouteValue("CustomerId"));
        }
    }
}
